function Stripe(config) {
    this.config = config;

    this.whatami = "StripeObject";
    
    this.type = undefined;
    this.element = undefined;
    
    this.add = function(toAdd) {
        if (toAdd.whatami == "StripeObject") { // if `toAdd` is a stripe object
            this.element.appendChild(toAdd.element);
        } else { // if `toAdd` is actually an array of Stripe Objects
            for (var i in toAdd) {
                this.element.appendChild(toAdd[i].element);
            }
        }
    };
    
            
    this.construct = function() {
        // Constructor
        var i, config;
        
        config = this.config;

        if (config.type != undefined) {
            if (config.type.indexOf(" | ") >= 0) {
                this.element = document.createElement(config.type.split(" | ")[0]);
                this.element.type = config.type.split(" | ")[1];
            } else {
                this.element = document.createElement(config.type);
            }

            this.type = config.type;
        }
        
        if (config.text != undefined)
            this.element.innerHTML = config.text;
        
        if (config.add != undefined) {
            for (i in config.add) {
                if (config.add[i] != undefined && config.add[i].whatami != undefined && config.add[i].whatami == "StripeObject")
                    this.element.appendChild(config.add[i].element);
                else
                    console.log("Array add[] only accepts StripeObject's\n\tIn\t " + JSON.stringify(this));
            }
        }
        if (config.id != undefined)
            this.element.id = config.id;
        if (config.class != undefined)
            this.element.className = config.class;
        if (config.click != undefined)
            this.element.href = config.click;
        if (config.placeholder != undefined)
            this.element.placeholder = config.placeholder;
        if (config.value != undefined)
            this.element.value = config.value;
        
        if (config.attr != undefined) {
            for (i in config.attr) {
                this.element[i] = config.attr[i];
            }
        }

        // Render
        if (config.render != undefined) {
            config.render.innerHTML = "";
            config.render.appendChild(this.element);
        }
    }
    
    this.construct(this.config);
    
    return this;
}